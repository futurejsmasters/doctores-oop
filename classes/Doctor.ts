class CodigoDeDoctor {
  public codigo: number;

  constructor(codigo: number) {
    this.codigo = codigo;
  }

  public formatearCodigo() {
    return 'CR' + this.codigo;
  }
}

class Doctor {
  public nombre: string;
  public apellido: string;
  public codigo: CodigoDeDoctor;

  constructor(nombre: string, apellido: string, codigo: number) {
    this.nombre = nombre;
    this.apellido = apellido;
    this.codigo = new CodigoDeDoctor(codigo);
  }

  public diagnosticar() {
    // Funciones dentro de clases no llevan palabra function
    return 'Estoy diagnosticando al paciente';
  }
}

// Require
// ES5
// module.exports = {
//   Doctor,
//   CodigoDeDoctor
// };
// module.exports = Doctor;
// module.exports = [];
// module.exports = 'hola';
// module.exports = 123;
// exports.Doctor = Doctor;
// exports.CodigoDeDoctor = CodigoDeDoctor;
// ES6
export default Doctor;
export { CodigoDeDoctor, Doctor };
