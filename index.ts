// ES5
// const { Doctor, CodigoDeDoctor } = require('./classes/Doctor');
// ES6
// Export default => import Doctor from './classes/Doctor';
// Export {} => import { CodigoDeDoctor } from './classes/Doctor';
// Export combinado => import Doctor, { CodigoDeDoctor } from './classes/Doctor';

import Doctor from './classes/Doctor';

class Neurologo extends Doctor {
  private _tomarRadiografia() {
    console.log('Tomando Radiografía...');
  }

  public diagnosticar() {
    this._tomarRadiografia();
    return 'Estoy diagnosticando al paciente';
  }
}

class Pediatra extends Doctor {
  public diagnosticar() {
    return 'Estoy diagnosticando al niño';
  }
}

function rand(min: number, max: number) {
  let randomNum = Math.random() * (max - min) + min;
  return Math.floor(randomNum);
}

const cantidadDeDoctores: number = 5;
const listaDeDoctores: Doctor[] = [];

while (listaDeDoctores.length < cantidadDeDoctores) {
  const radar = rand(0, 150);
  let doctor: Doctor | null = null;
  if (radar >= 0 && radar <= 50) {
    doctor = new Doctor('Diego', 'Maradona', radar);
  } else if (radar <= 100) {
    doctor = new Neurologo('Katie', 'Perry', radar);
  } else {
    doctor = new Pediatra('Karen', 'Kaka', radar);
  }

  listaDeDoctores.push(doctor);
}

for (const doctor of listaDeDoctores) {
  console.log(doctor.nombre + ' ' + doctor.apellido + ' ' + doctor.codigo);
  console.log(doctor.diagnosticar());
  console.log(' ');
}
